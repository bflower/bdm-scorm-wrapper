import getSdkInterpreter from '@b-flower/bdm-base-sdk'
import * as actions from './actions'

const startSdkInterpreter = getSdkInterpreter(actions)

export default startSdkInterpreter
