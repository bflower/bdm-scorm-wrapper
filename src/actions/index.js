import SCORM_API from '../scorm'


/**
 * start slideshow & scorm
 */
export function start(ctx, evObj) {
  const { slideshowStore } = evObj
  SCORM_API.start()

  const externalData = {
    learnerId: SCORM_API.get('cmi.learner_id'),
    learnerName: SCORM_API.get('cmi.learner_name'),
    language: SCORM_API.get('cmi.learner_preference.language'),
    maxTimeAllowed: SCORM_API.get('cmi.max_time_allowed'),
    scaledPassingScore: SCORM_API.get('cmi.scaled_passing_score'),
    mode: SCORM_API.get('cmi.mode'),
    credit: SCORM_API.get('cmi.credit'),
    suspendData: getSuspendData(),
  }

  slideshowStore.setExternalData(externalData)
}


/**
 * stop scorm
 */
export function stop() {
  SCORM_API.stop()
}

/**
 * @param ctx - interpreter context
 */
export function stopAsSuspend(ctx, evObj) {
  saveSuspend(ctx, evObj)
  SCORM_API.stop(true)
}

/**
 *
 */
export function setCompleted(ctx, evObj) {
  saveSuspend(ctx, evObj)
  SCORM_API.complete()
}

/**
 * @param {String} evObj.slideshowStore
 *
 */
export function setScore(ctx, evObj) {
  const { slideshowStore } = evObj
  saveSuspend(ctx, evObj)
  SCORM_API.setScore(slideshowStore.getScore())
}

/**
 * @param ctx - interpreter context
 * @param {String} evObj.slideshowStore
 */
export function setLocation(ctx, evObj) {
  const { slideshowStore } = evObj

  SCORM_API.setLocation(slideshowStore.getLocation())
  saveSuspend(ctx, evObj)
  SCORM_API.commit()
}

/**
 * @param ctx - interpreter context
 * @param {Object} evObj
 * @param {String} evObj.slideshowStore
 */
export function setInteraction(ctx, evObj) {
  const { slideshowStore } = evObj
  const {
    // slide,
    id,
    description,
    learnerResponse,
    correctResponse,
    result = SCORM_API.interaction.result.INTERACTION_RESULT_NEUTRAL,
    latency = null,
    weighting = null,
    objectiveId = null,
    dtmTime = new Date(),
  } = slideshowStore.getCurrentResponse()

  SCORM_API.interaction.setInteractionJSON(
    id,
    learnerResponse, // learner_response
    mapResult(result), // result field
    correctResponse, // correctResponse // should be on slide.interaction
    description, // description
    weighting, //weighting
    latency, //latency
    objectiveId, // objectiveid // should be on slide.interaction
    dtmTime //dtmTime
  )
  saveSuspend(ctx, evObj)
  SCORM_API.commit()
}

function mapResult(result) {
  if (result === undefined || result === null) return SCORM_API.interaction.result.INTERACTION_RESULT_NEUTRAL
  if (result === 0 || result === false) return SCORM_API.interaction.result.INTERACTION_RESULT_WRONG
  if (result === 1 || result === true) return SCORM_API.interaction.result.INTERACTION_RESULT_CORRECT
  if (typeof result === 'string') return result
  return ''
}


/**
 * restore suspend data
 */
function getSuspendData(/*ctx, evObj*/) {
  try {
    return JSON.parse(SCORM_API.getSuspendData())
  } catch (e) {
    return {}
  }
}

/**
 *
 */
function saveSuspend(ctx, evObj) {
  const { recordSuspendData } = ctx
  const { slideshowStore } = evObj
  if (recordSuspendData) {
    SCORM_API.saveSuspend(JSON.stringify(slideshowStore.getSuspendData()))
  } else {
    SCORM_API.saveSuspend(JSON.stringify({}))
  }
}
