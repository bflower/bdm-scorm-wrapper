import { scorm } from '@gamestdio/scorm'
import { ConvertMilliSecondsIntoSCORM2004Time } from './utils'
import {
  setInteraction,
  setInteractionChoice,
  setInteractionFillIn,
  setInteractionJSON,
  setInteractionLikert,
  setInteractionMatching,
  setInteractionNumeric,
  setInteractionPerformance,
  setInteractionSequencing,
  setInteractionTrueFalse,
} from './interaction'

import {
  // EXIT_LOGOUT, // deprecated
  EXIT_NORMAL_EXIT,
  EXIT_SUSPEND,
  // EXIT_TIMEOUT,
  ENTRY_ABINITIO,
  ENTRY_RESUME,
  ENTRY_NORMAL,
  COMPLETION_COMPLETED,
  COMPLETION_INCOMPLETE,
  // COMPLETION_NOT_ATTEMPTED,
  COMPLETION_UNKNOWN,
  SUCCESS_FAILED,
  SUCCESS_PASSED,
  // SUCCESS_UNKNOWN,
  INTERACTION_TYPE_TRUE_FALSE,
  INTERACTION_TYPE_CHOICE,
  INTERACTION_TYPE_FILL_IN,
  INTERACTION_TYPE_LONG_FILL_IN,
  INTERACTION_TYPE_MATCHING,
  INTERACTION_TYPE_PERFORMANCE,
  INTERACTION_TYPE_SEQUENCING,
  INTERACTION_TYPE_LIKERT,
  INTERACTION_TYPE_NUMERIC,
  INTERACTION_TYPE_JSON,
  INTERACTION_RESULT_CORRECT,
  INTERACTION_RESULT_WRONG,
  INTERACTION_RESULT_UNANTICIPATED,
  INTERACTION_RESULT_NEUTRAL,
} from './APIConstants'

/**
 * Dans notre modèle, c'est le xstate qui pilote les commandes scorm
 * La seule exception, c'est la réhydratation du state à l'init (redémarrage) du module
 *
 */

let startTimeStamp

// initialize connection with parent/opener windows
const API = {
  EXIT_NORMAL_EXIT,
  EXIT_SUSPEND,
  ENTRY_ABINITIO,
  ENTRY_RESUME,
  ENTRY_NORMAL,
  /**
   * use cases appels
   *
   *    * à l'ouverture du module
   */
  start() {
    //record the time that the learner started the SCO so that we can report the total time
    startTimeStamp = new Date()

    scorm.initialize()
    const completionStatus = scorm.get('cmi.completion_status')
    if (completionStatus === COMPLETION_UNKNOWN) {
      scorm.set('cmi.completion_status', COMPLETION_INCOMPLETE)
    }
  },

  setLocation(location) {
    scorm.set('cmi.location', String(location))
    scorm.commit()
  },

  complete() {
    scorm.set('cmi.completion_status', COMPLETION_COMPLETED)
    scorm.commit()
  },

  saveSuspend(suspendData) {
    scorm.set('cmi.suspend_data', suspendData)
  },
  /**
   * use cases appels
   *
   *  * si il y a un bouton exit dans le module et que l'utilisateur clique dessus
   *  * si l'utilisateur sort du module via fermeture onglet, navigateur
   */
  stop(suspend = false) {
    const completed =
      scorm.get('cmi.completion_status') === COMPLETION_COMPLETED

    //note use of short-circuit AND. If the user reached the end, don't prompt.
    //just exit normally and submit the results.
    if (completed === false && suspend !== false) {
      //set exit to suspend
      scorm.set('cmi.exit', EXIT_SUSPEND)
      //issue a suspendAll navigation request
      scorm.set('adl.nav.request', 'suspendAll')
    } else {
      //set exit to normal
      scorm.set('cmi.exit', EXIT_NORMAL_EXIT)
      //issue an exitAll navigation request
      scorm.set('adl.nav.request', 'exitAll')
    }

    //process the unload handler to close out the session.
    //the presense of an adl.nav.request will cause the LMS to
    //take the content away from the user.

    //record the session time
    const endTimeStamp = new Date()
    const totalMilliseconds = endTimeStamp.getTime() - startTimeStamp.getTime()
    const scormTime = ConvertMilliSecondsIntoSCORM2004Time(totalMilliseconds)
    scorm.set('cmi.session_time', scormTime)
    scorm.commit()

    scorm.terminate()
  },

  setScore({ raw, scaled, min = 0, max = 100, success }) {
    scorm.set('cmi.score.raw', String(raw))
    scorm.set('cmi.score.min', String(min))
    scorm.set('cmi.score.max', String(max))

    scorm.set('cmi.score.scaled', String(scaled))

    if (success !== undefined) {
      scorm.set('cmi.success_status', success ? SUCCESS_PASSED : SUCCESS_FAILED)
    }
    scorm.commit()
  },

  commit() {
    scorm.commit()
  },

  getSuspendData() {
    return scorm.get('cmi.suspend_data')
  },

  get(key) {
    return scorm.get(key)
  },

  interaction: {
    setInteraction,
    setInteractionChoice,
    setInteractionFillIn,
    setInteractionJSON,
    setInteractionLikert,
    setInteractionMatching,
    setInteractionNumeric,
    setInteractionPerformance,
    setInteractionSequencing,
    setInteractionTrueFalse,
    types: {
      INTERACTION_TYPE_TRUE_FALSE,
      INTERACTION_TYPE_CHOICE,
      INTERACTION_TYPE_FILL_IN,
      INTERACTION_TYPE_LONG_FILL_IN,
      INTERACTION_TYPE_MATCHING,
      INTERACTION_TYPE_PERFORMANCE,
      INTERACTION_TYPE_SEQUENCING,
      INTERACTION_TYPE_LIKERT,
      INTERACTION_TYPE_NUMERIC,
      INTERACTION_TYPE_JSON,
    },
    result: {
      INTERACTION_RESULT_CORRECT,
      INTERACTION_RESULT_WRONG,
      INTERACTION_RESULT_UNANTICIPATED,
      INTERACTION_RESULT_NEUTRAL,
    },
  },
}

export default API
