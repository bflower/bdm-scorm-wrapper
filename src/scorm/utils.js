export function notEmpty(v) {
  return v !== undefined && v !== null && v !== ''
}

//make sure all the criteria are met to allow this value to be an identifier
export function CreateValidIdentifier(v) {
  let str = String(v).trim()

  //if the string starts with "urn:" then it has special requirements
  if (str.toLowerCase().indexOf('urn:') === 0) {
    str = str.substr(4)
  }

  //urns may only contain the following characters: letters, numbers - ( ) + . : = @ ; $ _ ! * ' %
  //if anything else is found, replace it with _
  return str.replace(/[^\w\-()+.:=@;$_!*'%]/g, '_')
}

//SCORM requires time to be formatted in a specific way
export function ConvertMilliSecondsIntoSCORM2004Time(intTotalMilliseconds) {
  let ScormTime = ''

  const HUNDREDTHS_PER_SECOND = 100
  const HUNDREDTHS_PER_MINUTE = HUNDREDTHS_PER_SECOND * 60
  const HUNDREDTHS_PER_HOUR = HUNDREDTHS_PER_MINUTE * 60
  const HUNDREDTHS_PER_DAY = HUNDREDTHS_PER_HOUR * 24
  const HUNDREDTHS_PER_MONTH = HUNDREDTHS_PER_DAY * ((365 * 4 + 1) / 48)
  const HUNDREDTHS_PER_YEAR = HUNDREDTHS_PER_MONTH * 12

  //decrementing counter - work at the hundreths of a second level because that is all the precision that is required  var HUNDREDTHS_PER_SECOND = 100
  let HundredthsOfASecond = Math.floor(intTotalMilliseconds / 10)

  // assumed to be 12 "average" months
  const Years = Math.floor(HundredthsOfASecond / HUNDREDTHS_PER_YEAR)
  HundredthsOfASecond -= Years * HUNDREDTHS_PER_YEAR

  // assumed to be an "average" month (figures a leap year every 4 years) = ((365*4) + 1) / 48 days - 30.4375 days per month
  const Months = Math.floor(HundredthsOfASecond / HUNDREDTHS_PER_MONTH)
  HundredthsOfASecond -= Months * HUNDREDTHS_PER_MONTH

  // 24 hours
  const Days = Math.floor(HundredthsOfASecond / HUNDREDTHS_PER_DAY)
  HundredthsOfASecond -= Days * HUNDREDTHS_PER_DAY

  // 60 minutes
  const Hours = Math.floor(HundredthsOfASecond / HUNDREDTHS_PER_HOUR)
  HundredthsOfASecond -= Hours * HUNDREDTHS_PER_HOUR

  // 60 seconds
  const Minutes = Math.floor(HundredthsOfASecond / HUNDREDTHS_PER_MINUTE)
  HundredthsOfASecond -= Minutes * HUNDREDTHS_PER_MINUTE

  // 100 hundreths of a seconds
  const Seconds = Math.floor(HundredthsOfASecond / HUNDREDTHS_PER_SECOND)
  HundredthsOfASecond -= Seconds * HUNDREDTHS_PER_SECOND

  if (Years > 0) {
    ScormTime += Years + 'Y'
  }
  if (Months > 0) {
    ScormTime += Months + 'M'
  }
  if (Days > 0) {
    ScormTime += Days + 'D'
  }

  //check to see if we have any time before adding the "T"
  if (HundredthsOfASecond + Seconds + Minutes + Hours > 0) {
    ScormTime += 'T'

    if (Hours > 0) {
      ScormTime += Hours + 'H'
    }

    if (Minutes > 0) {
      ScormTime += Minutes + 'M'
    }

    if (HundredthsOfASecond + Seconds > 0) {
      ScormTime += Seconds

      if (HundredthsOfASecond > 0) {
        ScormTime += '.' + HundredthsOfASecond
      }

      ScormTime += 'S'
    }
  }

  if (ScormTime === '') {
    ScormTime = '0S'
  }

  ScormTime = 'P' + ScormTime

  return ScormTime
}

export function ConvertDateToIso8601TimeStamp(pDtm) {
  const dtm = new Date(pDtm)

  const Year = dtm.getFullYear()

  const Month = ZeroPad(dtm.getMonth() + 1, 2)
  const Day = ZeroPad(dtm.getDate(), 2)
  const Hour = ZeroPad(dtm.getHours(), 2)
  const Minute = ZeroPad(dtm.getMinutes(), 2)
  const Second = ZeroPad(dtm.getSeconds(), 2)

  const strTimeStamp =
    Year + '-' + Month + '-' + Day + 'T' + Hour + ':' + Minute + ':' + Second

  return strTimeStamp
}

export function ConvertIso8601TimeStampToDate(strTimeStamp) {
  strTimeStamp = new String(strTimeStamp)
  const ary = strTimeStamp.split(/[:T-]/)

  const Year = ary[0]
  const Month = ary[1] - 1
  const Day = ary[2]
  const Hour = ary[3]
  const Minute = ary[4]
  const Second = ary[5]

  return new Date(Year, Month, Day, Hour, Minute, Second, 0)
}

function ZeroPad(intNum, intNumDigits) {
  let strTemp = new String(intNum)
  const intLen = strTemp.length

  if (intLen > intNumDigits) {
    strTemp = strTemp.substr(0, intNumDigits)
  } else {
    for (let i = intLen; i < intNumDigits; i++) {
      strTemp = '0' + strTemp
    }
  }
  return strTemp
}
