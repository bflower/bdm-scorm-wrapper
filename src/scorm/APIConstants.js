export const VERSION = '3.8.2'

export const PREFERENCE_DEFAULT = 0
export const PREFERENCE_OFF = -1
export const PREFERENCE_ON = 1


export const MODE_NORMAL = 1
export const MODE_BROWSE = 2
export const MODE_REVIEW = 3

export const MAX_CMI_TIME = 36002439990 //max CMI Timespan 9999:99:99.99

export const NO_ERROR = 0
export const ERROR_LMS = 1
export const ERROR_INVALID_PREFERENCE = 2
export const ERROR_INVALID_NUMBER = 3
export const ERROR_INVALID_ID = 4
export const ERROR_INVALID_STATUS = 5
export const ERROR_INVALID_RESPONSE = 6
export const ERROR_NOT_LOADED = 7
export const ERROR_INVALID_INTERACTION_RESPONSE = 8

export const EXIT_TYPE_SUSPEND = 'SUSPEND'
export const EXIT_TYPE_FINISH = 'FINISH'
export const EXIT_TYPE_TIMEOUT = 'TIMEOUT'
export const EXIT_TYPE_UNLOAD = 'UNLOAD'

export const INTERACTION_RESULT_CORRECT = 'correct'
export const INTERACTION_RESULT_WRONG = 'incorrect'
export const INTERACTION_RESULT_UNANTICIPATED = 'unanticipated'
export const INTERACTION_RESULT_NEUTRAL = 'neutral'


export const INTERACTION_TYPE_TRUE_FALSE = 'true-false'
export const INTERACTION_TYPE_CHOICE = 'choice'
export const INTERACTION_TYPE_FILL_IN = 'fill-in'
export const INTERACTION_TYPE_LONG_FILL_IN = 'long-fill-in'
export const INTERACTION_TYPE_MATCHING = 'matching'
export const INTERACTION_TYPE_PERFORMANCE = 'performance'
export const INTERACTION_TYPE_SEQUENCING = 'sequencing'
export const INTERACTION_TYPE_LIKERT = 'likert'
export const INTERACTION_TYPE_NUMERIC = 'numeric'
export const INTERACTION_TYPE_JSON = 'other'

export const EXIT_LOGOUT = 'logout'
export const EXIT_SUSPEND = 'suspend'
export const EXIT_NORMAL_EXIT = 'normal'
export const EXIT_TIMEOUT = 'time-out'

export const SUCCESS_PASSED = 'passed'
export const SUCCESS_FAILED = 'failed'
export const SUCCESS_UNKNOWN = 'unknown'

export const COMPLETION_COMPLETED = 'completed'
export const COMPLETION_INCOMPLETE = 'incomplete'
export const COMPLETION_NOT_ATTEMPTED = 'not attempted'
export const COMPLETION_UNKNOWN = 'unknown'

export const ENTRY_ABINITIO = 'ab-initio'
export const ENTRY_RESUME = 'resume'
export const ENTRY_NORMAL = ''

export const SCORM2004_TLA_EXIT_MESSAGE = 'exit,message'
export const SCORM2004_TLA_EXIT_NO_MESSAGE = 'exit,no message'
export const SCORM2004_TLA_CONTINUE_MESSAGE = 'continue,message'
export const SCORM2004_TLA_CONTINUE_NO_MESSAGE = 'continue,no message'

export const SCORM2004_CREDIT = 'credit'
export const SCORM2004_NO_CREDIT = 'no-credit'

export const SCORM2004_BROWSE = 'browse'
export const SCORM2004_NORMAL = 'normal'
export const SCORM2004_REVIEW = 'review'


export const SCORM2004_NO_ERROR = '0'
export const SCORM2004_ERROR_INVALID_PREFERENCE = '-1'
export const SCORM2004_ERROR_INVALID_STATUS = '-2'
export const SCORM2004_ERROR_INVALID_SPEED = '-3'
export const SCORM2004_ERROR_INVALID_TIMESPAN = '-4'
export const SCORM2004_ERROR_INVALID_TIME_LIMIT_ACTION = '-5'
export const SCORM2004_ERROR_INVALID_DECIMAL = '-6'
export const SCORM2004_ERROR_INVALID_CREDIT = '-7'
export const SCORM2004_ERROR_INVALID_LESSON_MODE = '-8'
export const SCORM2004_ERROR_INVALID_ENTRY = '-9'

export const SCORM2004_TRUE = 'true'
export const SCORM2004_FALSE = 'false'

export const SCORM2004_EARLIEST_DATE = new Date('1/1/1900')
