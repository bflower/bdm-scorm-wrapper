import { INTERACTION_TYPE_TRUE_FALSE } from '../APIConstants'
import setInteraction from './set'



export default function setInteractionTrueFalse(
  id,
  learnerResponse,
  correct,
  correctResponse,
  description,
  weighting,
  latency,
  objectiveId,
  dtmTime
) {
  return setInteraction(
    id,
    learnerResponse,
    correct,
    correctResponse,
    description,
    weighting,
    latency,
    objectiveId,
    dtmTime,
    INTERACTION_TYPE_TRUE_FALSE
  )
}