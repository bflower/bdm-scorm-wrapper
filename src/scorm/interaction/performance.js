import { INTERACTION_TYPE_PERFORMANCE } from '../APIConstants'
import setInteraction from './set'

function mapResponse(v) {
  return `[.]${v.length > 250 ? v.substr(0, 250) : v}`
}


export default function setInteractionPerformance(
  id,
  learnerResponse,
  correct,
  correctResponse,
  description,
  weighting,
  latency,
  objectiveId,
  dtmTime
) {
  return setInteraction(
    id,
    mapResponse(learnerResponse),
    correct,
    mapResponse(correctResponse),
    description,
    weighting,
    latency,
    objectiveId,
    dtmTime,
    INTERACTION_TYPE_PERFORMANCE
  )
}