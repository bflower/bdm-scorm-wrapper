import { INTERACTION_TYPE_CHOICE } from '../APIConstants'
import setInteraction from './set'

function mapResponse(arr) {
  return arr.map(v => v.Long).join('[,]')
}

export default function setInteractionMultipleChoice(
  id,
  aryLearnerResponse,
  correct,
  aryCorrectResponse,
  description,
  weighting,
  latency,
  objectiveId,
  dtmTime
) {
  return setInteraction(
    id,
    mapResponse(aryLearnerResponse),
    correct,
    mapResponse(aryCorrectResponse),
    description,
    weighting,
    latency,
    objectiveId,
    dtmTime,
    INTERACTION_TYPE_CHOICE
  )
}
