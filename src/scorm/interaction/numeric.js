import { INTERACTION_TYPE_NUMERIC } from '../APIConstants'
import setInteraction from './set'
import { notEmpty } from '../utils'

function mapCorrect(v) {
  return notEmpty(v) ? `${v}[:]${v}` : ''
}


export default function setInteractionNumeric(
  id,
  learnerResponse,
  correct,
  correctResponse,
  description,
  weighting,
  latency,
  objectiveId,
  dtmTime
) {
  return setInteraction(
    id,
    learnerResponse,
    correct,
    mapCorrect(correctResponse),
    description,
    weighting,
    latency,
    objectiveId,
    dtmTime,
    INTERACTION_TYPE_NUMERIC
  )
}
