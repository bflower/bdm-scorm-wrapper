import { INTERACTION_TYPE_LIKERT } from '../APIConstants'
import setInteraction from './set'

function mapResponse(v) {
  return (v && v.Long !== undefined) || ''
}


export default function setInteractionLikert(
  id,
  learnerResponse,
  correct,
  correctResponse,
  description,
  weighting,
  latency,
  objectiveId,
  dtmTime
) {
  return setInteraction(
    id,
    mapResponse(learnerResponse),
    correct,
    mapResponse(correctResponse),
    description,
    weighting,
    latency,
    objectiveId,
    dtmTime,
    INTERACTION_TYPE_LIKERT
  )
}
