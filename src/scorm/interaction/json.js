import { INTERACTION_TYPE_JSON } from '../APIConstants'
import setInteraction from './set'

import { notEmpty } from '../utils'

export default function setInteractionJSON(
  id,
  learnerResponse,
  correct,
  correctResponse,
  description,
  weighting,
  latency,
  objectiveId,
  dtmTime
) {
  return setInteraction(
    id,
    JSON.stringify(learnerResponse),
    correct,
    notEmpty(correctResponse) ? JSON.stringify(correctResponse) : null,
    description,
    weighting,
    latency,
    objectiveId,
    dtmTime,
    INTERACTION_TYPE_JSON
  )
}
