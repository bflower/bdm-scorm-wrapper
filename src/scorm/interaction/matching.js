import { INTERACTION_TYPE_MATCHING } from '../APIConstants'
import setInteraction from './set'


function mapResponse(arr) {
  return arr.map(v => {
    return `${v.Source.Long}[.]${v.Target.Long}`
  }).join('[,]')
}


export default function setInteractionMatching(
  id,
  aryLearnerResponse,
  correct,
  aryCorrectResponse,
  description,
  weighting,
  latency,
  objectiveId,
  dtmTime
) {
  return setInteraction(
    id,
    mapResponse(aryLearnerResponse),
    correct,
    mapResponse(aryCorrectResponse),
    description,
    weighting,
    latency,
    objectiveId,
    dtmTime,
    INTERACTION_TYPE_MATCHING
  )
}