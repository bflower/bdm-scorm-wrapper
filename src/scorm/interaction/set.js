import { scorm } from '@gamestdio/scorm'
import * as CONSTANTS from '../APIConstants'
import {
  notEmpty,
  CreateValidIdentifier,
  ConvertMilliSecondsIntoSCORM2004Time,
  ConvertDateToIso8601TimeStamp,
} from '../utils'

const {
  INTERACTION_RESULT_CORRECT,
  INTERACTION_RESULT_WRONG,
  INTERACTION_RESULT_UNANTICIPATED,
  INTERACTION_RESULT_NEUTRAL,
} = CONSTANTS

const RESULT = {
  CORRECT: INTERACTION_RESULT_CORRECT,
  WRONG: INTERACTION_RESULT_WRONG,
  UNANTICIPATED: INTERACTION_RESULT_UNANTICIPATED,
  NEUTRAL: INTERACTION_RESULT_NEUTRAL,
}

export { RESULT }

/**
 * add interaction to CMI
 *
 * @param id (long_identifier_type (SPM: 4000), RW) Unique label for the interaction
 * @param learnerResponse (format depends on interaction type, RW) Data generated when a learner responds to an interaction
 * @param correct (“correct”, “incorrect”, “unanticipated”, “neutral”) or a real number with values that is accurate to seven significant decimal figures real. , RW) Judgment of the correctness of the learner
 * @param correctResponse (format depends on interaction type, RW) One correct response pattern for the interaction
 * @param description (localized_string_type (SPM: 250), RW) Brief informative description of the interaction
 * @param weighting (real (10,7), RW) Weight given to the interaction relative to other interactions
 * @param latency (timeinterval (second,10,2), RW) Time elapsed between the time the interaction was made available to the learner for response and the time of the first response
 * @param objectiveId (long_identifier_type (SPM: 4000), RW) Label for objectives associated with the interaction - only one objective
 * @param dtmTime (time(second,10,0), RW) Point in time at which the interaction was first made available to the learner for learner interaction and response
 * @param interactionType (“true-false”, “choice”, “fill-in”, “long-fill-in”, “matching”, “performance”, “sequencing”, “likert”, “numeric” or “other”, RW) Which type of interaction is recorded
 *
 */
export default function setInteraction(
  id,
  learnerResponse,
  correct,
  correctResponse,
  description,
  weighting,
  latency,
  objectiveId,
  dtmTime,
  interactionType
) {
  const interactionId = CreateValidIdentifier(id)
  //try to retrieve index of interaction based on type and id
  let interIdx = findInteractionIdx(interaction => (
    interaction.type === interactionType && interaction.id === interactionId
  ) )
  if (interIdx === false) interIdx = scorm.get('cmi.interactions._count')
  const interactionPrefix = `cmi.interactions.${interIdx}`

  let blnResult

  const strResult = [
    INTERACTION_RESULT_CORRECT,
    INTERACTION_RESULT_WRONG,
    INTERACTION_RESULT_UNANTICIPATED,
    INTERACTION_RESULT_NEUTRAL,
  ].includes(correct)
    ? correct
    : ''

  blnResult = scorm.set(interactionPrefix + '.id', interactionId)
  blnResult =
    scorm.set(interactionPrefix + '.type', interactionType) && blnResult
  blnResult =
    scorm.set(interactionPrefix + '.learner_response', learnerResponse) &&
    blnResult

  if (notEmpty(strResult)) {
    blnResult = scorm.set(interactionPrefix + '.result', strResult) && blnResult
  }
  if (notEmpty(correctResponse)) {
    blnResult =
      scorm.set(
        interactionPrefix + '.correct_responses.0.pattern',
        correctResponse
      ) && blnResult
  }
  if (notEmpty(description)) {
    blnResult =
      scorm.set(interactionPrefix + '.description', description) && blnResult
  }
  if (notEmpty(weighting)) {
    blnResult =
      scorm.set(interactionPrefix + '.weighting', weighting) && blnResult
  }
  if (notEmpty(latency)) {
    blnResult =
      scorm.set(
        interactionPrefix + '.latency',
        ConvertMilliSecondsIntoSCORM2004Time(latency)
      ) && blnResult
  }
  if (notEmpty(objectiveId)) {
    blnResult =
      scorm.set(interactionPrefix + '.objectives.0.id', objectiveId) &&
      blnResult
  }

  blnResult =
    scorm.set(
      interactionPrefix + '.timestamp',
      ConvertDateToIso8601TimeStamp(dtmTime)
    ) && blnResult

  return blnResult
}



function findInteractionIdx(fn) {
  const nb = scorm.get('cmi.interactions._count')
  for (let i = 0; i < nb; i++) {
    if (fn(getInteraction(i)) === true) return i
  }
  return false
}


/**
 *
 * @param {Integer} interIdx
 * @returns {Object} interaction
 */
function getInteraction(idx) {
  const interactionPrefix = `cmi.interactions.${idx}`
  return {
    id: scorm.get(`${interactionPrefix}.id`),
    type: scorm.get(`${interactionPrefix}.type`),
    learner_response: scorm.get(`${interactionPrefix}.learner_response`),
    result: scorm.get(`${interactionPrefix}.result`),
    correct_responses: scorm.get(`${interactionPrefix}.correct_responses.0.pattern`),
    description: scorm.get(`${interactionPrefix}.description`),
    weighting: scorm.get(`${interactionPrefix}.weighting`),
    latency: scorm.get(`${interactionPrefix}.latency`),
    objective_id: scorm.get(`${interactionPrefix}.objectives.0.id`),
    timestamp: scorm.get(`${interactionPrefix}.timestamp`),
  }
}
