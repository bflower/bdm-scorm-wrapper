import {
  INTERACTION_TYPE_FILL_IN,
  INTERACTION_TYPE_LONG_FILL_IN,
} from '../APIConstants'
import setInteraction from './set'



function getInteractionType(learnerResponse, correctResponse) {
  if (correctResponse.length > 250 || learnerResponse.length > 250) {
    return INTERACTION_TYPE_LONG_FILL_IN
  }
  return INTERACTION_TYPE_FILL_IN
}

function mapResponse(v) {
  return v.length > 4000 ? v.substr(0, 4000) : v
}

export default function setInteractionFillIn(
  id,
  learnerResponse,
  correct,
  correctResponse,
  description,
  weighting,
  latency,
  objectiveId,
  dtmTime
) {
  if (correctResponse.length > 4000) {
    correctResponse = correctResponse.substr(0, 4000)
  }

  return setInteraction(
    id,
    mapResponse(learnerResponse),
    correct,
    mapResponse(correctResponse),
    description,
    weighting,
    latency,
    objectiveId,
    dtmTime,
    getInteractionType(learnerResponse, correctResponse)
  )
}
