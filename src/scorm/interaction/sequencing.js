import { INTERACTION_TYPE_SEQUENCING } from '../APIConstants'
import setInteraction from './set'

function mapResponse(arr) {
  return arr.map(v => {
    return `${v.Source.Long}[.]${v.Target.Long}`
  }).join('[,]')
}


export default function setInteractionSequencing(
  id,
  aryLearnerResponse,
  correct,
  aryCorrectResponse,
  description,
  weighting,
  latency,
  objectiveId,
  dtmTime
) {
  return setInteraction(
    id,
    mapResponse(aryLearnerResponse),
    correct,
    mapResponse(aryCorrectResponse),
    description,
    weighting,
    latency,
    objectiveId,
    dtmTime,
    INTERACTION_TYPE_SEQUENCING
  )
}
