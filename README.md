# bdm-scorm-wrapper

SCORM wrapper pour les modules MROCS de b-eden.

## Installation

````
npm i bdm-scorm-wrapper
````

## Description
Ce wrapper utilise [@b-flower/bdm-base-sdk](https://www.npmjs.com/package/@b-flower/bdm-base-sdk).

Il construit un objet d'`actions` spécifique à `@b-flower/bdm-base-sdk` et exporte la fonction `startSdkInterperter`.

Ce module intègre [@gamestdio/scorm](https://www.npmjs.com/package/@gamestdio/scorm) comme wrapper scorm.


````javascript
// src/index.js
import getSdkInterpreter from '@b-flower/bdm-base-sdk'
import * as actions from './actions'

const startSdkInterpreter = getSdkInterpreter(actions)

export default startSdkInterpreter

````

Toute la logique d'intégration SCORM est faite au niveau des `actions`


Les actions sont un objet de callback d'évènement comprenant les fonctions suivantes:

* `start`
* `stop`
* `stopAsSuspend`
* `setCompleted`
* `setScore`
* `setLocation`
* `setInteraction`

Voir le `readme` du module [@b-flower/bdm-base-sdk](https://www.npmjs.com/package/@b-flower/bdm-base-sdk)


## Utilisation

Pour utiliser le scorm wrapper, vous devez l'intégrer au module `MROCS`.


````javascript
// dans le module MROCS - index.js
import React from 'react'
import { render } from 'react-dom'

...
// store de votre module MROCS
import store from 'src/store/slideshow.store'

// import du wrapper
import startSdkInterpreter  from '@b-flower/bdm-scorm-wrapper'

// démarrage sdkInterpreter avec le store MROCS en paramètre
startSdkInterpreter(store)

// App React du module MROCS
import App from './app'
...


render(
  <App />,
  document.getElementById('root')
)

````


## Parti pris

### Type d'intéraction

Pour le moment, ce module ne prend en charge que des intéractions de type `other` avec un formatage des `learner_response` et `correct_response` sur un formation `JSON`.


### Résultat d'intéraction

La gestion des résultats d'intéraction (propriété `result`) suit les règles suivantes:


* 0 et false sont traités comme `incorrect`
* 1 et true sont traités comme `correct`
* null et undefined sont traités comme `neutral`
* une valeur réelle entre 1 et 0 est casté en chaine de caractère
* une valeur réelle non comprise entre 1 et 0 est traitée comme `` (chaine vide)
* une chaine de caractère est passée telle quelle si elle fait partie des valeurs autorisées (correct|incorrect|unaticipated|neutral)
